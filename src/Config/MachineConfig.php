<?php
namespace Config;

use Models\Coin;
use Models\Slot;
use Models\Item;

class MachineConfig
{
    private $config;

    private $slots;
    private $initialCoins;
    private $allowedCoins;

    public function __construct()
    {
        if (!file_exists(__DIR__.'/machineItems.php')) {
            echo 'Configuration not found.';
            exit;
        }

        $this->config = require 'machineItems.php';

        $this->setSlots();
        $this->setAllowedCoins();
        $this->setInitialCoins();
    }

    public function setSlots()
    {
        if (!isset($this->config['slots']) || !count($this->config['slots'])) {
            echo 'Error : Slot Configuration.';
            exit;
        }

        $slots = [];
        foreach ($this->config['slots'] as $slot) {
            $items = [];

            if (count($slot['items'])) {
                foreach ($slot['items'] as $item) {
                    $items[] = new Item($item);
                }
            }

            $slots[] = new Slot($slot['id'], $slot['price'], $items, $slot['defaultName']);
        }

        $this->slots = $slots;
    }

    public function setAllowedCoins()
    {
        if (!isset($this->config['allowed_coins']) || !count($this->config['allowed_coins'])) {
            echo 'Error : Allowed Coins Configuration.';
            exit;
        }

        $allowedCoins = [];
        foreach ($this->config['allowed_coins'] as $coin) {
            $allowedCoins[] = new Coin($coin['sign'], $coin['amount']);
        }

        $this->allowedCoins = $allowedCoins;
    }

    public function setInitialCoins()
    {
        if (!isset($this->config['initial_coins']) || !count($this->config['initial_coins'])) {
            echo 'Error : Initial Coins Configuration.';
            exit;
        }

        $initialCoins = [];
        foreach ($this->config['initial_coins'] as $stack) {
            if (isset($stack['count']) && $stack['count'] > 0) {
                for ($i = 0; $i < $stack['count']; $i++) {
                    $initialCoins[] = new Coin($stack['sign'], $stack['amount']);
                }
            }
        }

        $this->initialCoins = $initialCoins;
    }

    /**
     * @return array
     */
    public function getSlots() : array
    {
        return $this->slots;
    }

    /**
     * @return array
     */
    public function getAllowedCoins() : array
    {
        return $this->allowedCoins;
    }

    /**
     * @return array
     */
    public function getInitialCoins() : array
    {
        return $this->initialCoins;
    }
}
