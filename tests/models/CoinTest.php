<?php
use Models\Coin;
use PHPUnit\Framework\TestCase;

class CoinTest extends TestCase
{
    private $coin;

    public function setUp() : void
    {
        $this->coin = new Coin('$2', 2.00);
    }

    public function testCoinGetSign()
    {
        $this->assertEquals($this->coin->getSign(), '$2');
        $this->assertIsString($this->coin->getSign());
    }

    public function testCoinGetAmount()
    {
        $this->assertEquals($this->coin->getAmount(), 2.00);
        $this->assertIsNumeric($this->coin->getAmount());
    }
}