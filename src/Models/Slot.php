<?php

namespace Models;

class Slot
{
    private $id;
    private $price;
    private $items;
    private $defaultName;

    public function __construct(int $id, float $price, array $items, string $defaultName = '')
    {
        $this->id    = $id;
        $this->price = $price;
        $this->items = $items;
        $this->defaultName = $defaultName;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return number_format($this->price, 2);
    }

    /**
     * @return int
     */
    public function getAvailability()
    {
        return count($this->items);
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $this->items = array_values($items);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return count($this->items) ? $this->items[0]->getName() : $this->defaultName;
    }
}

