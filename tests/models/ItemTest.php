<?php
use Models\Item;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    private $item;

    public function setUp() : void
    {
        $this->item = new Item('Snickers');
    }

    public function testItemGetName()
    {
        $this->assertEquals($this->item->getName(), 'Snickers');
        $this->assertIsString($this->item->getName());
    }
}