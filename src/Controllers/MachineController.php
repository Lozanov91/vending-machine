<?php
namespace Controllers;

use Config\MachineConfig;
use Services\MachineService;
use Validators\MachineValidator;

use Models\Machine;
use Models\Slot;
use Models\Coin;

class MachineController
{
    private $machine;

    private $config;
    private $service;
    private $validator;

    public function __construct(MachineConfig $config, MachineService $service, MachineValidator $validator)
    {
        $this->config    = $config;
        $this->service   = $service;
        $this->validator = $validator;

        $this->setMachineState(new Machine($this->config->getSlots(), $this->config->getInitialCoins()));
    }

    /**
     * @return mixed
     */
    public function getMachineState()
    {
        return $this->machine;
    }

    /**
     * @param Machine $machine
     */
    public function setMachineState(Machine $machine)
    {
        $this->machine = $machine;
    }

    public function vend()
    {
        $renderWelcomeOutput = true;

        while(true) {

            if ($renderWelcomeOutput === true) {
                $renderWelcomeOutput = false;
                $this->service->renderWelcome($this->config);
            }

            $this->service->renderInput();

            $input      = strtolower(trim(fgets(STDIN)));
            $machine    = $this->getMachineState();
            $validator  = $this->validator->validateInput($input);

            if ($validator['is_valid'] === true) {

                $data = $validator['data'];

                switch ($validator['action_type']) {

                    case 'slot':

                        $change     = $this->service->calculateChange($machine, $data);
                        $coinStack  = $this->service->generateCoinStack($machine);
                        $canVendSlot = $this->validator->validateCanHandleSlot($data, $machine);

                        if ($canVendSlot['is_valid'] === true) {

                            $isChange = $this->service->getChangeForSlot($change, $coinStack, []);
                            if ($isChange['is_valid'] == true) {
                                $itemName = $data->getName();
                                $machine  = $this->handleSlot($data, $machine, $isChange);

                                $this->service->renderVend($itemName, $isChange['data']['coins']);
                            } else {
                                $this->service->renderMessage($isChange['data']['message']);
                            }

                        } else {
                            $this->service->renderMessage($canVendSlot['data']['message']);
                        }

                        break;

                    case 'coin':

                        $machine = $this->handleCoin($data, $machine);
                        $this->service->renderTendered($machine);

                        break;
                }

                $this->setMachineState($machine);
            }
        }
    }

    /**
     * @param Slot $slot
     * @param Machine $machine
     * @param $canHandleSlot
     * @return Machine
     */
    public function handleSlot(Slot $slot, Machine $machine, $canHandleSlot)
    {
        $machine = $machine->mergeCoinsAfterPurchase()
                            ->removeChangeCoins($canHandleSlot['data']['coins'])
                            ->vendItemFromSlot($slot)
                            ->updateBalance();

        return $machine;
    }

    /**
     * @param Coin $coin
     * @param Machine $machine
     * @return Machine
     */
    public function handleCoin(Coin $coin, Machine $machine)
    {
        $machine = $machine->addUserCoin($coin);

        return $machine;
    }
}

