<?php
namespace Validators;

use Models\Slot;
use Models\Machine;

class MachineValidator
{
    private $availableSlots;
    private $allowedCoins;

    public function __construct(array $slots, array $coins)
    {
        $this->availableSlots   = $slots;
        $this->allowedCoins     = $coins;
    }

    /**
     * @return array
     */
    public function getEmptyValidator()
    {
        return [
            'is_valid'    => false,
            'action_type' => 'error',
            'data'        => null
        ];
    }

    /**
     * @param string $input
     * @return array
     */
    public function validateInput(string $input)
    {
        $validator = $this->getEmptyValidator();

        $isLookingForSlot = $this->startsWith($input, 'slot');
        $isLookingForCoin = $this->startsWith($input, '$') || $this->endsWith($input, 'c');

        if (!$isLookingForSlot && !$isLookingForCoin) {
            return $validator;
        }

        if ($isLookingForSlot) {
            $slotId = trim(substr($input, strlen('slot')));

            foreach ($this->availableSlots as $slot) {
                if ($slot->getId() == $slotId) {
                    $validator['is_valid']    = true;
                    $validator['action_type'] = 'slot';
                    $validator['data']        = $slot;
                }
            }

            if ($validator['is_valid'] == false) {
                echo PHP_EOL.'Invalid Slot.'.PHP_EOL;
            }
        }

        if ($isLookingForCoin) {
            foreach ($this->allowedCoins as $coin) {
                if (strtolower($coin->getSign()) == strtolower($input)) {
                    $validator['is_valid']    = true;
                    $validator['action_type'] = 'coin';
                    $validator['data']        = $coin;
                }
            }

            if ($validator['is_valid'] == false) {
                echo PHP_EOL.'Invalid Coin.'.PHP_EOL;
            }
        }

        return $validator;
    }

    /**
     * @param Slot $slot
     * @param Machine $machine
     * @return array
     */
    public function validateCanHandleSlot(Slot $slot, Machine $machine)
    {
        $validator = $this->getEmptyValidator();
        $pass      = true;

        if ($slot->getAvailability() < 1) {
            $pass = false;
            $validator['data'] = [
                'message' => 'Empty Slot',
            ];
        }

        if ($slot->getPrice() > $machine->getEntered()) {
            $pass = false;
            $validator['data'] = [
                'message' => 'Not Enough Balance.',
            ];
        }

        if ($pass === true) {
            $validator['is_valid']    = true;
            $validator['action_type'] = 'success';
        }

        return $validator;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public function startsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
}