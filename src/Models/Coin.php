<?php
namespace Models;

class Coin
{
    private $sign;
    private $amount;

    public function __construct(string $sign, float $amount)
    {
        $this->sign   = $sign;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return number_format($this->amount, 2);
    }
}
