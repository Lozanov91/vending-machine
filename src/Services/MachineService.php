<?php
namespace Services;

use Config\MachineConfig;
use Models\Machine;
use Models\Slot;
use Models\Coin;

class MachineService
{
    public function renderInput()
    {
        echo 'Enter = ';
    }

    /**
     * @param MachineConfig $config
     */
    public function renderWelcome(MachineConfig $config)
    {
        $render = PHP_EOL.'Welcome to the Vending Machine simulator!'.PHP_EOL;
        $render .= PHP_EOL.'The vending machine contains the following products:'.PHP_EOL.PHP_EOL;
        foreach ($config->getSlots() as $slot) {
            $render .= 'Slot '.$slot->getId().' - '.$slot->getAvailability().' x '.$slot->getName().' = '.$slot->getPrice().PHP_EOL;
        }
        $render .= PHP_EOL.'The vending machine accepts the following coins'.PHP_EOL;

        $renderCoins = array_map(function ($coin) {
            return $coin->getSign();
        }, $config->getAllowedCoins());

        $render .= implode(",", $renderCoins).PHP_EOL;
        $render .= PHP_EOL.'Please insert coins one at a time and pressing enter after each, e.g. $2 or 5c'.PHP_EOL;
        $render .= PHP_EOL.'To vend from a slot type slot command, e.g. slot 1'.PHP_EOL.PHP_EOL;

        echo $render;
    }

    /**
     * @param Machine $machine
     */
    public function renderTendered(Machine $machine)
    {
        echo PHP_EOL . 'Tendered : ' . $machine->getEntered() . PHP_EOL;
    }

    /**
     * @param $itemName
     * @param $changeCoins
     */
    public function renderVend($itemName, $changeCoins)
    {
        $render = PHP_EOL . 'Enjoy !' . PHP_EOL;
        $render .= PHP_EOL . 'Item = ' . $itemName . PHP_EOL;
        $render .= PHP_EOL . 'Change = ' . implode(",", $changeCoins) . PHP_EOL;

        echo $render;
    }

    /**
     * @param $message
     */
    public function renderMessage($message)
    {
        echo PHP_EOL . $message . PHP_EOL;
    }

    /**
     * @param $change
     * @param $coinStack
     * @param array $coinsReturned
     * @return array
     */
    public function getChangeForSlot($change, $coinStack, $coinsReturned = [])
    {
        $result = [];
        $change = number_format($change, 2);
        $remainingChange = $change;

        $hasMoreCoins = false;
        foreach ($coinStack as $sKey => $stack) {
            if ($sKey <= $remainingChange && $stack['count'] > 0) {
                $hasMoreCoins = true;

                $coinsReturned[] = $stack['sign'];
                $remainingChange = $remainingChange - $stack['amount'];
                $coinStack[$sKey]['count']--;

                break;
            }
        }

        // everything is OK, change gived back
        if ($remainingChange == '0.00') {
            $result['is_valid'] = true;
            $result['action_type'] = 'success';
            $result['data'] = [
                'coins' => count($coinsReturned) ? $coinsReturned : ['0.00'],
            ];

            return $result;
        }

        // not enough appropriate coins
        if ($hasMoreCoins === false) {
            $result['is_valid'] = false;
            $result['action_type'] = 'error';
            $result['data'] = [
                'message' => 'Unable to give change back.',
                'change'  => $remainingChange,
            ];

            return $result;
        }

        return $this->getChangeForSlot($remainingChange, $coinStack, $coinsReturned);
    }

    /**
     * @param Machine $machine
     * @return array
     */
    public function generateCoinStack(Machine $machine)
    {
        $stack      = [];
        $allCoins   = array_merge($machine->getMachineCoins(), $machine->getUserCoins());

        foreach ($allCoins as $coin) {
            $key = $coin->getAmount();

            if ( !isset( $stack[$key] ) ) {
                $stack[$key] = [
                    'sign' => $coin->getSign(),
                    'amount' => $coin->getAmount(),
                    'count' => 1
                ];
            } else {
                $stack[$key]['count'] += 1;
            }
        }

        return $stack;
    }

    /**
     * @param Machine $machine
     * @param Slot $slot
     * @return string
     */
    public function calculateChange(Machine $machine, Slot $slot)
    {
        return number_format($machine->getEntered(), 2) - number_format($slot->getPrice(), 2);
    }
}
