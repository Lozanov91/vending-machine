<?php

require __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL);

use Config\MachineConfig;
use Validators\MachineValidator;
use Services\MachineService;

use Controllers\MachineController;

$config     = new MachineConfig();
$service    = new MachineService();
$validator  = new MachineValidator($config->getSlots(), $config->getAllowedCoins());
$controller = new MachineController($config, $service, $validator);

$controller->vend();

// The machine should vend if :
// 1. There is stock in the slot
// 2. The deposit is equal or greater than the selected item
// 3. The machine is able to give back remaining change if the deposit is greater than cost of the item

// Rules to use the vending machine:

// 1 - enter coin and hit ENTER (example : $2)
//      output : Tendered = 2.00

// 2 - enter slot and hit ENTER (example : slot 1)
//      output : Enjoy !
//             : Item = Mars
//             : change = 20c, 5c

//dd($slots, $coins, $_SESSION);