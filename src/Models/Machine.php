<?php
namespace Models;

class Machine
{
    private $slots          = [];
    private $userCoins      = [];
    private $machineCoins   = [];

    private $amount         = 0;
    private $entered        = 0;

    public function __construct(array $slots, array $machineCoins)
    {
        $this->slots        = $slots;
        $this->machineCoins = $machineCoins;

        $this->amount = $this->calculateCoins($machineCoins);
    }

    /**
     * @param Coin $coin
     * @return $this
     */
    public function addUserCoin(Coin $coin)
    {
        $this->userCoins[] = $coin;
        $this->entered = $this->calculateCoins($this->userCoins);

        return $this;
    }

    /**
     * @param array $coins
     * @return int
     */
    public function calculateCoins(array $coins)
    {
        $total = 0;

        if (count($coins)) {
            foreach ($coins as $coin) {
                $total += $coin->getAmount();
            }
        }

        return $total;
    }

    /**
     * @return $this
     */
    public function mergeCoinsAfterPurchase()
    {
        $allCoins = array_merge($this->machineCoins, $this->userCoins);

        $this->setMachineCoins($allCoins);
        $this->emptyUserCoins();

        return $this;
    }

    /**
     * @param $coins
     * @return $this
     */
    public function removeChangeCoins($coins)
    {
        $machineCoins = $this->machineCoins;

        foreach ($coins as $coinSign) {
            foreach ($machineCoins as $k => $machineCoin) {
                if ($coinSign == $machineCoin->getSign()) {
                    unset($machineCoins[$k]);
                    break;
                }
            }
        }

        $this->setMachineCoins($machineCoins);

        return $this;
    }

    /**
     * @param $slot
     * @return $this
     */
    public function vendItemFromSlot($slot)
    {
        $allSlots = $this->slots;

        foreach ($allSlots as $k => $iterationSlot) {
            if ($slot->getId() == $iterationSlot->getId()) {
                $slotItems = $allSlots[$k]->getItems();
                unset($slotItems[0]);
                $allSlots[$k]->setItems($slotItems);
            }
        }

        $allSlots = array_values($allSlots);
        $this->slots = $allSlots;

        return $this;
    }

    /**
     * @return array
     */
    public function getUserCoins()
    {
        return $this->userCoins;
    }

    /**
     * @return array
     */
    public function getMachineCoins()
    {
        return $this->machineCoins;
    }

    /**
     * @return string
     */
    public function getEntered()
    {
        return number_format($this->entered, 2);
    }

    /**
     * @param $coins
     */
    public function setMachineCoins($coins)
    {
        $this->machineCoins = $coins;
    }

    public function emptyUserCoins()
    {
        $this->userCoins = [];
    }

    /**
     * @return $this
     */
    public function updateBalance()
    {
        $this->amount  = $this->calculateCoins($this->machineCoins);
        $this->entered = $this->calculateCoins($this->userCoins);

        return $this;
    }
}

