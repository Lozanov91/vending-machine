<?php

return [
    'allowed_coins' => [
        ['sign' => '$2', 'amount' => 2.00],
        ['sign' => '$1', 'amount' => 1.00],
        ['sign' => '50c', 'amount' => 0.50],
        ['sign' => '20c', 'amount' => 0.20],
        ['sign' => '10c', 'amount' => 0.10],
        ['sign' => '5c', 'amount' => 0.05],
    ],
    'initial_coins' => [
        ['count' => '2', 'sign' => '$2', 'amount' => 2.00],
        ['count' => '4', 'sign' => '$1', 'amount' => 1.00],
        ['count' => '2', 'sign' => '50c', 'amount' => 0.50],
        ['count' => '3', 'sign' => '20c', 'amount' => 0.20],
        ['count' => '4', 'sign' => '10c', 'amount' => 0.10],
        ['count' => '1', 'sign' => '5c', 'amount' => 0.05],
    ],
    'slots' => [
        ['id' => 1, 'price' => 1.05, 'items' => ['Snickers','Snickers','Snickers'], 'defaultName' => 'Snickers'],
        ['id' => 2, 'price' => 1.00, 'items' => [], 'defaultName' => 'Bounty'], // Bounty - 0
        ['id' => 3, 'price' => 1.25, 'items' => ['Mars'], 'defaultName' => 'Mars'],
        ['id' => 4, 'price' => 2.00, 'items' => ['Twix', 'Twix'], 'defaultName' => 'Twix'],
        ['id' => 5, 'price' => 1.80, 'items' => ['Wispa', 'Wispa'], 'defaultName' => 'Wispa'],
        ['id' => 6, 'price' => 0.75, 'items' => ['Twirl', 'Twirl'], 'defaultName' => 'Twirl'],
        ['id' => 7, 'price' => 1.80, 'items' => ['Yorkie', 'Yorkie', 'Yorkie'], 'defaultName' => 'Yorkie'],
        ['id' => 8, 'price' => 1.80, 'items' => [], 'defaultName' => 'Aero'], // Aero - 0
        ['id' => 9, 'price' => 0.75, 'items' => ['Double Decker', 'Double Decker', 'Double Decker'], 'defaultName' => 'Double Decker'],
        ['id' => 10, 'price' => 1.80, 'items' => ['Galaxy', 'Galaxy'], 'defaultName' => 'Galaxy'],
        ['id' => 11, 'price' => 1.80, 'items' => ['Crunchie', 'Crunchie', 'Crunchie',], 'defaultName' => 'Crunchie'],
        ['id' => 12, 'price' => 1.25, 'items' => ['Picnic', 'Picnic'], 'defaultName' => 'Picnic'],
        ['id' => 13, 'price' => 2.00, 'items' => ['Kit Kat', 'Kit Kat'], 'defaultName' => 'Kit Kat'],
        ['id' => 14, 'price' => 1.80, 'items' => ['Lion Bar', 'Lion Bar', 'Lion Bar'], 'defaultName' => 'Lion Bar'],
        ['id' => 15, 'price' => 2.00, 'items' => ['Oreo', 'Oreo'], 'defaultName' => 'Oreo'],
        ['id' => 16, 'price' => 2.00, 'items' => ['Toffee Crisp'], 'defaultName' => 'Toffee Crisp'],
        ['id' => 17, 'price' => 1.50, 'items' => ['Boost'], 'defaultName' => 'Boost'],
    ],
];