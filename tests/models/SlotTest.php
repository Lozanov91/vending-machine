<?php

use Models\Slot;
use PHPUnit\Framework\TestCase;

class SlotTest extends TestCase
{
    private $slot;

    public function setUp() : void
    {
        $this->slot = new Slot(1, 1.50, []);
    }

    public function testSlotGetId()
    {
        $this->assertEquals($this->slot->getId(),1);
        $this->assertIsNumeric($this->slot->getId());
    }

    public function testSlotGetPrice()
    {
        $this->assertEquals($this->slot->getPrice(), 1.50);
        $this->assertIsNumeric($this->slot->getPrice());
    }

    public function testSlotGetAvailability()
    {
        $this->assertEquals(0, $this->slot->getAvailability());
    }

    public function testSlotGetItems()
    {
        $this->assertEmpty($this->slot->getItems());
    }

    public function testSlotGetName()
    {
        $this->assertEquals($this->slot->getName(), '');
        $this->assertIsString($this->slot->getName());
    }

    public function testSlotSetItems()
    {
        $insert = ['Snickers', 'Twix'];
        $this->slot->setItems($insert);

        $this->assertEquals($insert, $this->slot->getItems());
        $this->assertIsArray($this->slot->getItems());
    }
}